FROM debian:latest

RUN apt -y update
RUN apt -y upgrade
RUN apt install curl autoconf pkg-config libcurl4-openssl-dev libjansson-dev libssl-dev libgmp-dev make g++ wget ca-certificates -y
RUN curl -sSL https://get.haskellstack.org/ | sh
RUN ln -s /usr/local/lib/libjansson.so.4 /usr/lib/libjansson.so.4


USER container
ENV  USER container
ENV HOME /home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
